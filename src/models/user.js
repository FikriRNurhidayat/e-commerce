const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const userSchema = new Schema({
  email: {
    type: 'string',
    required: true,
    unique: true
  },
  password: {
    type: 'string',
    required: true
  }
});

var User = mongoose.model('User', userSchema);

User.register = function(data) {
  return new Promise(async (resolve, reject) => {
    switch(true) {
      case (!data.email):
        reject([400, "Email is required!"])
        break;
      case (!data.password):
        reject([400, "Password is required!"]);
        break;
      case (data.password != data.password_confirmation):
        reject([400, "Password doesn't match!"]);
    };

    try {
      let encryptedPassword = await bcrypt.hash(data.password, 10);
      let user = await User.create({
        email: data.email,
        password: encryptedPassword
      })

      let token = await jwt.sign({ _id: user._id }, process.env.SECRET_KEY);
      resolve([201, {
        _id: user._id,
        email: user.email,
        token: token
      }])
    }

    catch(err) {
      reject([422, err]);
    }

  })
}

User.authenticate = function(data) {
  return new Promise(async (resolve, reject) => {
    let user = await User.findOne({ email: data.email }).select(['_id','email','password'])
    if (!user) return reject([422, "Email has't registered yet!"]);

    let isValid = await bcrypt.compare(data.password, user.password);
    console.log(isValid);
    if (isValid) {
      let token = await jwt.sign({ _id: user._id }, process.env.SECRET_KEY);
      resolve([201, token]);
    } else {
      reject([401, "Wrong Password!"]);
    }
  })
}

module.exports = User;
