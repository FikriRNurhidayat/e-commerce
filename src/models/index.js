const fs = require('fs');
const path = require('path');

const current_dir = path.join(__dirname);

// Capitalize
function capitalize(data) {
  return data.charAt(0).toUpperCase() + data.slice(1);
}

// Initialize models object;
var models = {};

// Read all file on models directory
var files = fs.readdirSync(current_dir);
var model = files.filter(file => file.search('.swp') < 0 && file != 'index.js');

// Filtering models that we want to use
model.forEach(file => {
  let filename = file.split('.')[0];
  models[capitalize(filename)] = require(`./${file}`);
});

module.exports = models;
