const fs = require('fs');
const path = require('path');

const current_dir = path.join(__dirname);

// Read all file on middlewares directory
var files = fs.readdirSync(current_dir);
var middleware = files.filter(file => file.search('.swp') < 0 && file != 'index.js');
var middlewares = {}

// Filtering middlewares that we want to use
middleware.forEach(file => {
  let filename = file.split('.')[0];
  middlewares[filename] = require(`./${file}`);
});

module.exports = middlewares;
