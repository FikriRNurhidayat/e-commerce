const { error } = require('../helpers/responseFormatter.js');
const { User } = require('../models');
const jwt = require('jsonwebtoken');

module.exports = async function(req, res, next) {
  let token = req.headers.authorization;

  switch(true) {
    case (!token):
      return error([res, 401, "No token provided!"])
      break;
    case (token.split(" ").length < 2):
      return error([res, 401, "Invalid token format!"])
      break;
    case (token.split(" ")[0] !== "Bearer"):
      return error([res, 401, "Token require 'Bearer' prefix!"])
  }

  try {
    token = token.split(" ")[1];
    let payload = await jwt.sign(token, process.env.SECRET_KEY);
    let user = await User.findById(payload._id).select(['_id','email']);
    req.headers.authorization = payload;
    next()
  }

  catch(err) {
    console.log(err);
    error([res, 401, err])
  }
}
