const { User } = require('../models');
const { success, error } = require('../helpers/responseFormatter');

module.exports = {
  
  async register(req, res) {
    try {
      let data = await User.register(req.body)
      data.unshift(res);
      success(data);
    }

    catch(err) {
      err.unshift(res);
      error(err);
    }
  },

  async login(req, res) {
    try {
      let data = await User.authenticate(req.body);
      data.unshift(res);
      success(data)
    }

    catch(err) {
      err.unshift(res);
      error(err);
    }
  },

  async whoAmI(req, res) {
    let data = [200, req.headers.authorization]
    data.unshift(res);
    success(data);
  }
}
