const fs = require('fs');
const path = require('path');

const current_dir = path.join(__dirname);

// Initialize models object;
var controllers = {};

// Read all file on models directory
var files = fs.readdirSync(current_dir);
var controller = files.filter(file => file.search('.swp') < 0 && file != 'index.js');

// Filtering models that we want to use
controller.forEach(file => {
  let filename = file.split('.')[0];
  controllers[filename] = require(`./${file}`);
});

module.exports = controllers;
