// Declare router as usual
const router = require('express').Router();
const fs = require('fs');
const path = require('path');

const current_dir = path.join(__dirname);

/* istanbul ignore next */
function isEmpty(obj) {
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      return false;
    }
  }

  return true;
}

// Initialize models object;
var routers = [];

// Read all file on models directory
var files = fs.readdirSync(current_dir);
var routes = files.filter(file => file.search('.swp') < 0 && file != 'index.js');

/* istanbul ignore next */
routes.forEach(file => {
  let data = require(`./${file}`);
  if (!isEmpty(data)) {
    routers.push(data);
  }
})

/* istanbul ignore next */
routers.forEach(route => {
  router.use(route[0], route[1])
});

module.exports = router;
