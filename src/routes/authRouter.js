const { 
  register,
  login,
  whoAmI
} = require('../controllers').authController;
const { authenticate } = require('../middlewares')
const router = require('express').Router();

router.post('/register', register);
router.post('/login', login);
router.get('/', authenticate, whoAmI);

module.exports = ['/auth', router];
