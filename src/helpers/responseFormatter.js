module.exports = {
  success(data) {
    data[0].status(data[1]).json({
      success: true,
      data: data[2]
    })
  },

  error(data) {
    data[0].status(data[1]).json({
      success: false,
      errors: data[2]
    })
  }
}
