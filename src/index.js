// Initialization of Express
const express = require('express');
const app = express();

// Environment Setting
/* istanbul ignore next */
const env = process.env.NODE_ENV || 'development';
/* istanbul ignore if */
if (env == 'development') {
  require('dotenv').config();
}

// Import middleware
const morgan = require('morgan');
const connectDatabase = require('./config/database.js');

/* istanbul ignore next */
connectDatabase(env)
  .then(data => console.log(`Database Connected!`))
  .catch(err => console.log(err));

// Third-Party Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static('public'));
app.use(morgan('dev'));

app.get('/', (req, res) => {
  res.status(200).json({
    success: true,
    data: {
      message: "Welcome to Root API",
      environment: env
    }
  })
})

app.use('/api/v1', require('./routes'))

module.exports = app;
