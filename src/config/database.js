const mongoose = require('mongoose');

const connectionString = {
  development: process.env.DATABASE_CONNECTION + '_development',
  test: process.env.DATABASE_CONNECTION + '_test',
  staging: process.env.DATABASE_CONNECTION + '_staging',
  production: process.env.DATABASE_CONNECTION + '_production',
}

module.exports = function(env) {
  return mongoose.connect(connectionString[env], { 
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
}
