const chai = require('chai');
const server = require('../src');
const expect = chai.expect;

chai.use(require('chai-http'));

describe('Root API', () => {
  it("Should Return Success", done => {
    chai
      .request(server)
      .get('/')
      .end(function(err, res) {
        expect(res.body.success).to.equal(true);
        expect(res.body.data.environment).to.equal("test");
        done();
      })
  })
});
