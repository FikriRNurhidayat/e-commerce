const chai = require('chai');
const expect = chai.expect;
const server = require('../../../../src/index.js')
const faker = require('faker');
chai.use(require('chai-http'));

const { User } = require('../../../../src/models');

describe("GET /api/v1/auth", () => {
  let email = faker.internet.email();
  let password = faker.internet.password();
  let password_confirmation = password;
  
  let data = {
    email: email,
    password: password,
    password_confirmation: password_confirmation
  }
  
  beforeEach(done => {
    User.register(data).then(user => {
      data.token = user[1].token
      done()
    });
  })

  afterEach(done => {
    User.deleteMany({}).then(data => done());
  })
  
  it("Should get current user information", done => {
    chai
      .request(server)
      .get('/api/v1/auth')
      .set('Authorization',`Bearer ${data.token}`)
      .end(function(err, res) {
        expect(res.body.success).to.equal(true);
        done();
      })
  })
  
  it("No token error!", done => {
    chai
      .request(server)
      .get('/api/v1/auth')
      .end(function(err, res) {
        expect(res.body.success).to.equal(false);
        expect(res.body.errors).to.equal("No token provided!")
        done();
      })
  })

  it("Wrong token format error!", done => {
    chai
      .request(server)
      .get('/api/v1/auth')
      .set('Authorization', data.token)
      .end(function(err, res) {
        expect(res.body.success).to.equal(false);
        expect(res.body.errors).to.equal("Invalid token format!")
        done();
      })
  })

  it("Wrong token prefix error!", done => {
    chai
      .request(server)
      .get('/api/v1/auth')
      .set('Authorization', `${faker.internet.email()} ${data.token}`)
      .end(function(err, res) {
        expect(res.body.success).to.equal(false);
        expect(res.body.errors).to.equal("Token require 'Bearer' prefix!")
        done();
      })
  })
})
