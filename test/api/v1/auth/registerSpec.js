const chai = require('chai');
const expect = chai.expect;
const server = require('../../../../src/index.js')
const faker = require('faker');
chai.use(require('chai-http'));

const { User } = require('../../../../src/models');

describe("POST /api/v1/auth/register", () => {
  
  let email = faker.internet.email();
  let password = faker.internet.password();
  let password_confirmation = password;
  
  let data = {
    email: email,
    password: password,
    password_confirmation: password_confirmation
  }

  afterEach(done => {
    User.deleteMany({}).then(data => done());
  })

  it("Should create new user", done => {
    chai
      .request(server)
      .post('/api/v1/auth/register')
      .set('Content-Type','application/json')
      .send(JSON.stringify(data))
      .end(function(err, res) {
        expect(res.body.success).to.equal(true);
        done()
      })
  })

  context("User Field", () => {
    
    it("Should not create because of blank password", done => {
      let blankPassword = {
        email: data.email,
        password_confirmation: data.password_confirmation
      }
      chai
        .request(server)
        .post('/api/v1/auth/register')
        .set('Content-Type','application/json')
        .send(JSON.stringify(blankPassword))
        .end(function(err, res) {
          expect(res.body.success).to.equal(false);
          expect(res.body.errors).to.equal("Password is required!")
          done()
        })
    })
    
    it("Should not create because of blank email", done => {
      let blankEmail = {
        password: data.password,
        password_confirmation: data.password_confirmation
      }

      chai
        .request(server)
        .post('/api/v1/auth/register')
        .set('Content-Type','application/json')
        .send(JSON.stringify(blankEmail))
        .end(function(err, res) {
          expect(res.body.success).to.equal(false);
          expect(res.body.errors).to.equal("Email is required!")
          done()
        })
    })

    it("Should not create because password doesn't match", done => {
      data.password = faker.internet.password();
      chai
        .request(server)
        .post('/api/v1/auth/register')
        .set('Content-Type','application/json')
        .send(JSON.stringify(data))
        .end(function(err, res) {
          expect(res.body.success).to.equal(false);
          expect(res.body.errors).to.equal("Password doesn't match!")
          done()
        })
    })
  })


  it("Should not create user because of duplication", done => {
    User.register(data).then(user => {
      chai
        .request(server)
        .post('/api/v1/auth/register')
        .set('Content-Type','application/json')
        .send(JSON.stringify(data))
        .end(function(err, res) {
          expect(res.body.success).to.equal(false);
          expect(res.body.errors).to.have.property('errmsg')
          done()
        }) 
    })
  })
})
