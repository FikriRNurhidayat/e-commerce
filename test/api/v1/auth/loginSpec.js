const chai = require('chai');
const expect = chai.expect;
const server = require('../../../../src/index.js')
const faker = require('faker');
chai.use(require('chai-http'));

const { User } = require('../../../../src/models');

describe("POST /api/v1/auth/login", () => {
  
  let email = faker.internet.email();
  let password = faker.internet.password();
  let password_confirmation = password;
  
  let data = {
    email: email,
    password: password,
    password_confirmation: password_confirmation
  }
  
  beforeEach(done => {
    User.register(data).then(data => {
      done()
    });
  })

  afterEach(done => {
    User.deleteMany({}).then(data => done());
  })

  it("Should successfully login", done => {
    chai
      .request(server)
      .post('/api/v1/auth/login')
      .set('Content-Type','application/json')
      .send(JSON.stringify(data))
      .end(function(err, res) {
        expect(res.status).to.equal(201);
        expect(res.body.success).to.equal(true);
        expect(res.body.data).to.be.a("string");
        done();
      })
  })

  it("Should not logged in because of wrong password", done => {
    let wrongPassword = {
      email: data.email,
      password: faker.internet.password()
    };

    chai
      .request(server)
      .post('/api/v1/auth/login')
      .set('Content-Type','application/json')
      .send(JSON.stringify(wrongPassword))
      .end(function(err, res) {
        expect(res.status).to.equal(401);
        expect(res.body.success).to.equal(false);
        expect(res.body.errors).to.equal("Wrong Password!");
        done();
      })
  })

  it("Should not logged in because of email hasn't registered yet", done => {
    let unregisteredEmail = {
      email: faker.internet.email(),
      password: faker.internet.password()
    };

    chai
      .request(server)
      .post('/api/v1/auth/login')
      .set('Content-Type','application/json')
      .send(JSON.stringify(unregisteredEmail))
      .end(function(err, res) {
        expect(res.status).to.equal(422);
        expect(res.body.success).to.equal(false);
        done();
      })
  })
})
